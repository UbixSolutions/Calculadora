﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculadora.Forms
{
    public class LogDbServico : ILog
    {
        public void GravarLog(Log log)
        {
            using (var context = new CalculadoraContext())
            {
                context.Logs.Add(log);
                context.SaveChanges();
            }
        }
    }
}
