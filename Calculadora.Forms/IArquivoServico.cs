﻿namespace Calculadora.Forms
{
    public interface IArquivoServico
    {
        void Salvar(string texto, string path);
    }
}