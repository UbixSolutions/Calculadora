﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculadora.Forms
{
    public class Subtracao : ICalculo
    {
        public decimal Calcular(decimal valor1, decimal valor2)
        {
            return valor1 - valor2;
        }
    }
}
