﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculadora.Forms
{
    public class Configuration : DbMigrationsConfiguration<CalculadoraContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }
    }

    public class CalculadoraContext : DbContext
    {
        public CalculadoraContext() : base(@"Data Source=C:\Users\otavio.fevereiro\Desktop\temp\CalculadoraData.sdf;Persist Security Info=False;")
        {

        }

        public DbSet<Log> Logs { get; set; }
    }

    public class Log
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public decimal Valor1 { get; set; }
        public decimal Valor2 { get; set; }
        public Operador Operacao { get; set; }
    }
}
