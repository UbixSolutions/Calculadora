﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculadora.Forms
{

    public class LogTxtServico : ILog
    {
        public LogTxtServico(IArquivoServico arquivoServico)
        {
            this.arquivoServico = arquivoServico;
        }

        public const string logPath = @"C:\Users\otavio.fevereiro\Desktop\temp\calculadora_log.txt";

        readonly IArquivoServico arquivoServico;

        public void GravarLog(Log log)
        {
            arquivoServico.Salvar($"[{log.Data}] [Operação: {log.Operacao.ToString()}] [Primeiro Valor: {log.Valor1.ToString()}] [Segundo Valor: { log.Valor2.ToString()}]", logPath);
        }
    }
}
