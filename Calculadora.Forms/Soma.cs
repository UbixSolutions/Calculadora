﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculadora.Forms
{
    public class Soma : ICalculo
    {
        public decimal Calcular(decimal valor1, decimal valor2)
        {
            return valor1 + valor2;
        }
    }
}
