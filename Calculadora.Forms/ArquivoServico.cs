﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculadora.Forms
{
    public class ArquivoServico : IArquivoServico
    {
        public void Salvar(string texto, string path)
        {
            using (var file = new StreamWriter(path, true))
            {
                file.WriteLine(texto);
            }
        }
    }
}
