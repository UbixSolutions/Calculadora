﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculadora.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calculadora.Tests
{
    [TestClass]
    public class CalculoFabricaTest
    {

        [TestMethod]
        public void Criar_UtilizandoOperadorDivisao_EsperadaClasseDivisaoImplementada()
        {
            //Arrange 
            Operador operador = Operador.Divisao;

            //Act
            var divisaoServico = CalculoFabrica.Criar(operador);

            //Assert
            Assert.AreEqual(typeof(Divisao), divisaoServico.GetType());
        }

        [TestMethod]
        public void Criar_UtilizandoOperadorMultiplicacao_EsperadaClasseMultiplicacaoImplementada()
        {
            //Arrange 
            Operador operador = Operador.Multiplicacao;

            //Act
            var multiplicacaoServico = CalculoFabrica.Criar(operador);

            //Assert
            Assert.AreEqual(typeof(Multiplacacao), multiplicacaoServico.GetType());
        }

        [TestMethod]
        public void Criar_UtilizandoOperadorSoma_EsperadaClasseSomaImplementada()
        {
            //Arrange 
            Operador operador = Operador.Soma;

            //Act
            var somaServico = CalculoFabrica.Criar(operador);

            //Assert
            Assert.AreEqual(typeof(Soma), somaServico.GetType());
        }

        [TestMethod]
        public void Criar_UtilizandoOperadorSubtracao_EsperadaClasseSubtracaoImplementada()
        {
            //Arrange 
            Operador operador = Operador.Subtracao;

            //Act
            var subtracaoServico = CalculoFabrica.Criar(operador);

            //Assert
            Assert.AreEqual(typeof(Subtracao), subtracaoServico.GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(NotImplementedException), "Não implementado")]
        public void Criar_UtilizandoOperadorInexsitente_DeveRetornarExcecao()
        {
            //Arrange 
            Operador operador = Operador.RaizQuadrada;

            //Act
            CalculoFabrica.Criar(operador);

            //Assert
            // Esperado Exceção
        }
    }
}
