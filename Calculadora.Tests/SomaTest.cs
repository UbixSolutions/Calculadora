﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calculadora.Forms;

namespace Calculadora.Tests
{
    [TestClass]
    public class SomaTest
    {
        [TestMethod]
        public void Calcular_SomarDoisValores_EsperadoResultadoCorreto()
        {
            //Arrange
            decimal valor1 = 1;
            decimal valor2 = 2;
            var calculo = new Soma();

            //Act
            decimal resultado = calculo.Calcular(valor1, valor2);

            //Assert
            Assert.AreEqual(3, resultado);
        }

        [TestMethod]
        public void Calcular_SomarDoisValoresDecimais_EsperadoResultadoCorreto()
        {
            //Arrange
            decimal valor1 = 1.5M;
            decimal valor2 = 2.2M;
            var calculo = new Soma();

            //Act
            decimal resultado = calculo.Calcular(valor1, valor2);

            //Assert
            Assert.AreEqual(3.7M, resultado);
        }

        [TestMethod]
        public void Calcular_SomarDoisValoresNegativos_EsperadoResultadoCorreto()
        {
            //Arrange
            decimal valor1 = -50;
            decimal valor2 = -60;
            var calculo = new Soma();

            //Act
            decimal resultado = calculo.Calcular(valor1, valor2);

            //Assert
            Assert.AreEqual(-110, resultado);
        }
    }
}
