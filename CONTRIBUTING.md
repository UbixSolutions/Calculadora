Calculadora
Realiza operações de Soma, Subtração, Divisão e Multiplicação.
Deve salvar log em arquivo TXT e/ou Banco de Dados via Entity Framework.
Deve ser coberto por Testes Automatizados.
Deve estar de acordo com os Princípios de POO e do SOLID.
